#!/bin/bash

port=8083
host=localhost

# Post account and return Id
function postAccount {
    account=$(curl -sb -H "Accept: application/json" -X  POST "${host}:${port}/accounts/account" -d '
    {
        "name": "'$1'",
        "currency": "'$2'"
    }' 2>/dev/null)
    echo $account | awk -F"\"" '{print $4}'
}

# Get Account given Id
function getAccount {
    account=$(curl -sb -H "Accept: application/json" -X  GET "${host}:${port}/accounts/$1" 2>/dev/null)
    echo $account
}

# Deposit Account given Id and Amount
function depositAccount {
    deposit=$(curl -sb -H "Accept: application/json" -X  POST "${host}:${port}/transactions/deposit" -d '
    {
        "destinationId": "'$1'",
        "amount": '$2'
    }' 2>/dev/null)
    echo ${deposit} | awk -F"\"" '{print $22}'
}
# Create transaction with source, destination Id's and amount
function postTransaction {
    transaction=$(curl -sb -H "Accept: application/json" -X  POST "${host}:${port}/transactions/transaction" -d '
    {
        "sourceId": "'$1'",
        "destinationId": "'$2'",
        "amount": '$3'
    }' 2>/dev/null)
    echo ${transaction} | awk -F"\"" '{print $38}'
}

# Confirm transaction
function confirmTransaction {
    transaction=$(curl -sb -H "Accept: application/json" -X  POST "${host}:${port}/transactions/confirm" -d '
    {
        "transactionId": "'$1'"
    }' 2>/dev/null)
    echo ${transaction}
}



##### MAIN START HERE #######

port=$1
host=$2

printf "\n\nPOST accounts\n"

accountA=$(postAccount "Market" "USD")
accountB=$(postAccount "Bank" "USD")

echo ${accountA}
echo ${accountB}

printf "\n\nGET accounts\n"
accounts=$(curl -sb -H "Accept: application/json" -X  GET "${host}:${port}/accounts" 2>/dev/null)
echo ${accounts}

printf "\n\nGET account by Id\n"
echo $(getAccount ${accountA})
echo $(getAccount ${accountB})


printf "\n\nPOST deposit transaction\n"
depositTransactionId=$(depositAccount ${accountA} 400)
printf "\n\n Deposit Transaction Id\n"
echo ${depositTransactionId}

depositTransaction=$(confirmTransaction ${depositTransactionId})
printf "\n\n Deposit Transaction after Confirm\n"
echo $depositTransaction

printf "\n\nPOST transactions\n"
transferTransactionId=$(postTransaction ${accountA} ${accountB} 200)
echo $transferTransactionId
transferTransaction=$(confirmTransaction ${transferTransactionId})
echo $transferTransaction


###
printf "\n\nGET transactions\n"
transactions=$(curl -sb -H "Accept: application/json" -X  GET "${host}:${port}/transactions" 2>/dev/null)
echo ${transactions}