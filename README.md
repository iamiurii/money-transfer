#Money-Transfer Service
HTTP Rest API for money transfers between accounts.

Create Account
```
/accounts/account - POST
```
Get Account
```
/accounts/{accountId} - GET
```
Create Deposit Account Transaction
```
/transactions/deposit - POST
```
Create Transfer Transaction (between Accounts)
```
/transactions/transaction - POST
```
Confirm Transaction
```
/transactions/confirm - POST 
```
Get Accounts
```
/accounts - GET
```
Get Transactions 
```
/transactions - GET
```

Check examples in curl_requests.sh

##Run with gradle
```
gradle run
```
##Build fat jar
```
gradle customFatJar
```
##Run service
```
java -jar ./build/libs/MoneyTransfer.jar -port 8089 -host localhost
```
##Curl tests
```
./curl_requests.sh 8089 localhost
```