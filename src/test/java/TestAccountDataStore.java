import java.math.BigDecimal;
import org.junit.Test;
import org.junit.*;
import static org.junit.Assert.*;

public class TestAccountDataStore {

    private static DataStore<Account, String> dataStore;
    
    @BeforeClass
    public static void initDataStore() {
        dataStore = new DataStore<>();
    }

    @Test
    public void testAddAccount() throws NoSuchInstanceException {
        Account account = new Account();
        dataStore.addInstance(account);
        Account accountFromStore = dataStore.getInstance(account.getId());
        assertEquals(accountFromStore.getId(), account.getId());
        assertEquals(accountFromStore.getBalance(), account.getBalance());
    }
}
