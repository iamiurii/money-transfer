import java.math.BigDecimal;
import org.junit.Test;
import static org.junit.Assert.*;

public class TransactionTest {

    private String sourceNbr = "1234";
    private String destinationNbr = "1235";
    private BigDecimal sourceBalance = new BigDecimal(5.49);
    private BigDecimal destinationBalance = new BigDecimal(15.23);
    private BigDecimal amount = new BigDecimal(3);

    @Test
    public void testTransaction() {   
        Account sourceAccount = new Account();
        Account destinationAccount = new Account();
        sourceAccount.deposit(sourceBalance);
        destinationAccount.deposit(destinationBalance);

        ICommand transfer = new Transaction(sourceAccount, destinationAccount, amount);
        transfer.perform();
        
        assertEquals(sourceBalance.subtract(amount), sourceAccount.getBalance());
        assertEquals(destinationBalance.add(amount), destinationAccount.getBalance());
    }
}