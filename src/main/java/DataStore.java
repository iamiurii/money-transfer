import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.ArrayList;

interface IUniqueInstance<Key> {
    Key getId();
}

class DataStore <Type extends IUniqueInstance<Key>, Key> implements IDataStore<Type, Key> {

    private Hashtable<Key, Type> hashTable;
    private final static String NO_SUCH_INSTANCE = "No such instance in Data Store";
    public DataStore() {
        this.hashTable = new Hashtable<>();
    }
    public ArrayList<Type> getInstances (IInstancesSpecification<Type> spec) {
        ArrayList<Type> resultList = new ArrayList<>();
        for (Key key : hashTable.keySet()) {
            Type instance = hashTable.get(key);
            if (spec.isSatisfied(instance))
                resultList.add(instance);
        }
        return resultList;
    }
    public Type getInstance(Key key) throws NoSuchInstanceException {
        Type instance = hashTable.get(key);
        if (null == instance)
            throw new NoSuchInstanceException(NO_SUCH_INSTANCE);
        else
            return instance;
    }
    public void addInstance(Type instance) {
        hashTable.put(instance.getId(), instance);
    }
}