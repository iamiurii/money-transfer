import org.apache.commons.cli.*;

public class App {

    public static void main(final String[] args) {
        int port;
        String host;

        Options options = new Options();
        options.addOption("host", true, "hostname for service");
        options.addOption("port", true, "port for service");

        CommandLineParser parser = new BasicParser();
        
        try {
            CommandLine cmd = parser.parse(options, args);
            host = cmd.getOptionValue("host", "localhost");
            port = Integer.parseInt(cmd.getOptionValue("port", "8081"));
            if (port < 1024 || port > 65535)
                throw new ParseException("Port should be integer value in [1024,65535] range");
        } catch(ParseException ex) {
            System.err.println(ex.getMessage());
            return;
        }

        Logger.Log().write("Transfer-Money Service Start\n" + " - Host: " + host + "\n - Port: " + port + "\n");

        DataStore<Transaction, String> transactionDs = new DataStore<>();
        DataStore<Account, String> accountDs = new DataStore<>();

        HandlingQueue handlingQueue = new HandlingQueue(500);

        Thread queueProcessThread = new Thread() {
            @Override
            public void run() {
                try {
                    handlingQueue.processCommands();
                } catch (InterruptedException e) {
                    System.out.println("implement-me-please");
                }
            }
        };
        queueProcessThread.start();

        TransactionManager manager = new TransactionManager()
            .setQueue(handlingQueue)
            .setTransactionDs(transactionDs)
            .setAccountDs(accountDs);

            // Initialize main service module
            TransferService service = new TransferService(port, host)
                .setTransactionManager(manager);
            service.start();

            try {
                queueProcessThread.join();
            } catch (InterruptedException e) {
                System.out.println("implement-me-please");
            }
    }
}