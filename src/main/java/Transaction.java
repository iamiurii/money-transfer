import java.math.BigDecimal;
import java.util.*;

class UnknownTransactionStateException extends Exception {
    public UnknownTransactionStateException (String message) {
        super(message);
    }
}

interface ICommand {
    void perform();
    default void onQueueingFailed() {}
}

interface ISpecification<Type> {
    boolean isValid(Type object);
}

class EnoughFundsSpecification implements ISpecification<Transaction> {
    public boolean isValid(Transaction transaction) {
        BigDecimal balance = transaction.getSource().getBalance();
        BigDecimal amount = transaction.getAmount();
        boolean valid = true;
        if (balance.compareTo(amount) < 0)
            valid = false;
        return valid;
    }
}

class Transaction implements ICommand, IUniqueInstance<String> {
    protected Account source;
    protected Account destination;
    protected BigDecimal amount;
    protected String id;
    protected Date date;
    protected TransactionState transactionState;
    
    public Transaction (Account source, Account destination,
                        BigDecimal amount) {
        this.source = source;
        this.destination = destination;
        this.amount = amount;
        this.id = UUID.randomUUID().toString();
        this.date = new Date();
        this.transactionState = TransactionState.ACTIVE;
    }

    public String getId() {
        return id;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public Account getSource() {
        return source;
    }

    public void perform() {
        try {
            source.withdraw(amount);
            destination.deposit(amount);
            transactionState = TransactionState.COMMITED;
        } catch(WithdrawalException e) {
            Logger.Log().write("implement-me");
        } catch(IllegalArgumentException e) {
            Logger.Log().write("implement-me");
        }
    }
    public void onQueueingFailed () {
        transactionState = TransactionState.FAILED;
    }
    public void validate(ISpecification spceification) {
        if (spceification.isValid(this))
            transactionState = TransactionState.VALIDATED;
        else
            transactionState = TransactionState.FAILED;
    }
    public String toString() {
        return id + "," + source + "," + destination + "," +
               amount + "," + date + "," + getState();
    }
    enum TransactionState {
        ACTIVE,
        VALIDATED,
        FAILED,
        COMMITED
    }
    public String getState() {
        String stateDescription = null;
        switch(transactionState) {
            case ACTIVE:
                stateDescription = "ACTIVE";
                break;
            case VALIDATED:
                stateDescription = "VALIDATED";
                break;
            case FAILED:
                stateDescription = "FAILED";
                break;
            case COMMITED:
                stateDescription = "COMMITED";
                break;
        }
        return stateDescription;
    }
}

class DepositTransaction extends Transaction {
    public DepositTransaction (Account destination, BigDecimal amount) {
        super(null, destination, amount);
    }
    @Override
    public void perform() {
        try {
            destination.deposit(amount);
            transactionState = TransactionState.COMMITED;
        } catch(IllegalArgumentException e) {
            Logger.Log().write("implement-me");
        }
    }
}