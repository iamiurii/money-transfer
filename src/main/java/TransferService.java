import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.BlockingHandler;
import io.undertow.server.RoutingHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import java.util.ArrayList;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

class TransferService {
    private Undertow server;
    private HttpHandler routes;
    private DataStore<Account, String> accountDs;
    private DataStore<Transaction, String> transactionDs;
    private TransactionManager transactionManager;
    private Gson gson = new Gson();

    private final static int BAD_REQUEST = 400;
    private final static int NOT_FOUND = 404;

    private String getParam (HttpServerExchange exchange, String paramName) {
        String res = null;
        for (String param : exchange.getQueryParameters().keySet()) {
            if (param.equals(paramName)) {
                res = exchange.getQueryParameters().get(paramName).removeLast();
                break;
            }
        }
        return res;
    }

    private static <T> T constructFromJsonBody (HttpServerExchange exchange, Class<T> type) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(exchange.getInputStream()));
        T account = (new Gson()).fromJson(reader, type);
        return account;
    }

    public TransferService(final int port, final String host) {
        this.routes = new RoutingHandler()
                    .get("/accounts", (HttpServerExchange exchange)->
                    {
                        ArrayList<Account> list = transactionManager.getAccounts(new AllSpecification<Account>());
                        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
                        exchange.getResponseSender().send((new Gson()).toJson(list));
                    })

                    .post("/accounts/account", new BlockingHandler((HttpServerExchange exchange)-> {
                        
                        try {
                            Account account = TransferService.<Account>constructFromJsonBody(exchange, Account.class);
                            transactionManager.addAccount(account);
                            exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
                            exchange.getResponseSender().send((new Gson()).toJson(account));
                        } catch(IllegalArgumentException|JsonSyntaxException ex) {
                            Logger.Log().write(ex.getMessage());
                            Logger.Log().write("Unable to parse Account json");
                            exchange.setStatusCode(BAD_REQUEST);
                        }
                    }))

                    .get("/accounts/{accountId}", (HttpServerExchange exchange)->
                    {
                        if (1 == exchange.getQueryParameters().size()) {
                            String accountId = getParam(exchange, "accountId");
                            if (null != accountId) {
                                Account account = transactionManager.getAccount(accountId);
                                if (null != account) {
                                    exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
                                    exchange.getResponseSender().send((new Gson()).toJson(account));
                                } else
                                    exchange.setStatusCode(NOT_FOUND);
                            }
                        } else {
                            exchange.setStatusCode(BAD_REQUEST);
                        }
                        
                    })
                    .post("/transactions/deposit", new BlockingHandler((HttpServerExchange exchange)->
                    {
                        DepositTransactionMessage message = TransferService.
                                    <DepositTransactionMessage>constructFromJsonBody(exchange, DepositTransactionMessage.class);
                        Transaction transaction = transactionManager.addDepositTransaction(message);
                        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
                        exchange.getResponseSender().send((new Gson()).toJson(transaction));

                    }))
                    .post("/transactions/confirm", new BlockingHandler((HttpServerExchange exchange)->
                    {
                        ConfirmTransactionMessage message = TransferService.
                                    <ConfirmTransactionMessage>constructFromJsonBody(exchange, ConfirmTransactionMessage.class);
                        Transaction transaction = transactionManager.confirmTransaction(message.transactionId);
                        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
                        exchange.getResponseSender().send((new Gson()).toJson(transaction));

                    }))
                    .get("/transactions", (HttpServerExchange exchange)->
                    {
                        ArrayList<Transaction> list = transactionManager.getTransactions(new AllSpecification<Transaction>());
                        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
                        exchange.getResponseSender().send((new Gson()).toJson(list));
                    })

                    .post("/transactions/transaction", new BlockingHandler((HttpServerExchange exchange)-> {
                        try {
                            TransactionCreateMessage message = TransferService.
                                    <TransactionCreateMessage>constructFromJsonBody(exchange, TransactionCreateMessage.class);
                            Transaction transaction = transactionManager.addTransaction(message);
                            exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
                            exchange.getResponseSender().send((new Gson()).toJson(transaction));
                        } catch(IllegalArgumentException ex) {
                            Logger.Log().write(ex.getMessage());
                            Logger.Log().write("Unable to parse Account json");
                            exchange.setStatusCode(BAD_REQUEST);
                        }
                    }))

                    .get("/transactions/{transactionId}", (HttpServerExchange exchange)->
                    {
                        String transactionId = null;
                        if (1 == exchange.getQueryParameters().size()) {
                            for (String key : exchange.getQueryParameters().keySet()) {
                                Logger.Log().write(key);
                                if (key.equals("transactionId")) {
                                    transactionId = exchange.getQueryParameters().get(key).removeLast();
                                }
                            }
                            if (null != transactionId) {
                                exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
                                try {
                                    exchange.getResponseSender().send((new Gson()).toJson(transactionManager.getTransaction(transactionId)));
                                } catch(NoSuchInstanceException ex) {
                                    exchange.setStatusCode(NOT_FOUND);
                                }
                            }
                        } else {
                            Logger.Log().write("return baad request");
                            exchange.setStatusCode(BAD_REQUEST);
                        }
                        
                    })

                    .setFallbackHandler(BasicRouting::notFound);

        this.server = Undertow.builder().addHttpListener(port, host)
                                            .setHandler(routes)
                                            .build();

    }

    public TransferService setTransactionManager (TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
        return this;
    }

    public void start() {
        server.start();
    }

}

class BasicRouting {
    public static void notFound (HttpServerExchange exchange) {
        exchange.setStatusCode(404);
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
        exchange.getResponseSender().send("{\"page\": \"not found\"}");
    }
}
