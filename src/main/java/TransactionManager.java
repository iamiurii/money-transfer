import java.util.*;
import java.math.BigDecimal;

class TransactionCreateMessage {
    public String sourceId;
    public String destinationId;
    public BigDecimal amount;
}

class DepositTransactionMessage {
    public String destinationId;
    public BigDecimal amount;
}

class ConfirmTransactionMessage {
    public String transactionId;
}

class TransactionManager {

    private HandlingQueue handlingQueue;
    private DataStore<Transaction, String> transactionDs;
    private DataStore<Account, String> accountDs;

    public TransactionManager setQueue(HandlingQueue handlingQueue) {
        this.handlingQueue = handlingQueue;
        return this;
    }
    public TransactionManager setTransactionDs(DataStore<Transaction, String> transactionDs) {
        this.transactionDs = transactionDs;
        return this;
    }
    public TransactionManager setAccountDs(DataStore<Account, String> accountDs) {
        this.accountDs = accountDs;
        return this;
    }

    public Account getAccount (String accountId) {
        Account account = null;
        try {
            account = accountDs.getInstance(accountId);
        } catch(NoSuchInstanceException ex) {
            Logger.Log().write("ERROR: Could not fetch Account. Account is unknown");
            Logger.Log().write(ex.getMessage());
        }
        return account;
    }
    public void addAccount (Account account) {
        accountDs.addInstance(account);
    }
    public ArrayList<Account> getAccounts (IInstancesSpecification<Account> spec) {
        ArrayList<Account> accounts = accountDs.getInstances(spec);
        return accounts;
    }
    public Transaction addTransaction (TransactionCreateMessage message) {
        Transaction transaction = null;
        try {
            Account source = accountDs.getInstance(message.sourceId);
            Account destination = accountDs.getInstance(message.destinationId);
            transaction = new Transaction(source, destination, message.amount);
            transaction.validate(new EnoughFundsSpecification());
            transactionDs.addInstance(transaction);
        } catch(NoSuchInstanceException ex) {
            Logger.Log().write("ERROR: Could not create Transaction. Account(s) missed");
            Logger.Log().write(ex.getMessage());
        }

        return transaction;
    }
    
    public DepositTransaction addDepositTransaction (DepositTransactionMessage message) {
        DepositTransaction transaction = null;
        try {
            Account destination = accountDs.getInstance(message.destinationId);
            transaction = new DepositTransaction(destination, message.amount);
            transactionDs.addInstance(transaction);
        } catch(NoSuchInstanceException ex) {
            Logger.Log().write("ERROR: Could not create Transaction. Account missed");
            Logger.Log().write(ex.getMessage());
        }

        return transaction;
    }
    public Transaction confirmTransaction (String transactionId) {
        Transaction transaction = null;
        try {
            transaction = transactionDs.getInstance(transactionId);
            handlingQueue.addCommand (transaction);
        } catch(NoSuchInstanceException ex) {
            Logger.Log().write("ERROR: Could not confirm Transaction. Transaction Id is unknown");
            Logger.Log().write(ex.getMessage());
        }
        return transaction;
    }
    public Transaction getTransaction (String transactionId) throws NoSuchInstanceException {
        Transaction transaction = null;
        try {
            transaction = transactionDs.getInstance(transactionId);
        } catch(NoSuchInstanceException ex) {
            Logger.Log().write("ERROR: Could not fetch Transaction. Transaction Id is unknown");
            Logger.Log().write(ex.getMessage());
        }
        return transaction;
    }
    public ArrayList<Transaction> getTransactions (IInstancesSpecification<Transaction> spec) {
        ArrayList<Transaction> transactions = transactionDs.getInstances(spec);
        return transactions;
    }
}