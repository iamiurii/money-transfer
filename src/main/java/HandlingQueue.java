import java.util.concurrent.*;


class CloseQueueCommand implements ICommand {
    private HandlingQueue queueHandler;
    public CloseQueueCommand(HandlingQueue queueHandler) {
        this.queueHandler = queueHandler;
    }
    public void perform() {
        queueHandler.stopHandling();
    }
}

class HandlingQueue {
    private LinkedBlockingQueue<ICommand> queue;
    private int maxQueueSize;
    private boolean shouldClose = false;

    public HandlingQueue(final int maxQueueSize) {
        this.maxQueueSize = maxQueueSize;
        this.queue = new LinkedBlockingQueue<>(maxQueueSize);
    }

    public void addCommand(ICommand command) {
        try {
            queue.put(command);
        } catch(InterruptedException ex) {
            command.onQueueingFailed();
        }
    }

    public void processCommands() throws InterruptedException {
        while (false == shouldClose) {
            queue.take().perform();
        }
    }

    public void stopHandling() {
        shouldClose = true;
    }
}