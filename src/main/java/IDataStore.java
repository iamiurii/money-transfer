import java.util.ArrayList;

class NoSuchInstanceException extends Exception {
    public NoSuchInstanceException (String message) {
        super(message);
    }
}
interface IInstancesSpecification<Type> {
    boolean isSatisfied(Type object);
}
interface IDataStore<Type, Key> {
    ArrayList<Type> getInstances (IInstancesSpecification<Type> spec);
    Type getInstance(Key key) throws NoSuchInstanceException;
    void addInstance(Type instance);
}