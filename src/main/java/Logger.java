import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

class Writer {
    private BufferedWriter fileWriter;

    public Writer(final String file) {
        try {
            this.fileWriter = new BufferedWriter(new FileWriter(file, true));
        } catch(IOException ex) {
            System.err.println("ERROR opening " + file);
        }
    }

    public synchronized void write (String record) {
        try {
            fileWriter.write(record);
            fileWriter.newLine();
            fileWriter.flush();
        } catch (IOException e) {
            System.err.println("ERROR logging message: " + record);
        } 
    }
}

class Logger {
    private static Writer logWriter = new Writer("application.log");
    private static Writer journalWriter = new Writer("journal.log");
    public static Writer Log() {
        return logWriter;
    }
    public static Writer Journal() {
        return journalWriter;
    }
}