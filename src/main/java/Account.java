import java.math.BigDecimal;
import java.util.UUID;
import java.util.Currency;


class AccountOwner {}

class Account implements IUniqueInstance<String> {
    private String id;
    private String name;
    private BigDecimal balance;
    private Currency currency;
    private AccountOwner owner;
    public final static String ILLEGAL_ARG_MESSAGE = "Negative amount for withdrawal.";
    public final static String INSUFFICIENT_BALANCE_MESSAGE = "Insufficient balance.";

    public Account (final String id) {
        this.id = id;
    }
    public Account() {
        this.id = UUID.randomUUID().toString();
        this.balance = new BigDecimal(0);
    }
    public Account Name(String name) {
        this.name = name;
        return this;
    }
    public Account Currency(Currency currency) {
        this.currency = currency;
        return this;
    }
    public Account Owner(AccountOwner owner) {
        this.owner = owner;
        return this;
    }
    public BigDecimal getBalance () {
        return balance;
    }
    public String getId() {
        return id;
    }
    public AccountOwner getOwner() {
        return owner;
    }
    public Currency getCurrency() {
        return currency;
    }
    public String toString() {
        StringBuilder sBr = new StringBuilder();
        sBr.append(id).append(":").append(name).append(":").append(currency);
        return sBr.toString();
    }
    public void withdraw (BigDecimal amount) throws WithdrawalException {
        if (amount.compareTo(BigDecimal.ZERO) < 0)
            throw new IllegalArgumentException(ILLEGAL_ARG_MESSAGE);
        else if (balance.compareTo(amount) < 0)
            throw new WithdrawalException(INSUFFICIENT_BALANCE_MESSAGE);
        else
            balance = balance.subtract(amount);
    }
    
    public void deposit (BigDecimal amount) {
        balance = balance.add(amount);
    }
}